class Event < ApplicationRecord
  validates :artist, :presence => true
  validates :price, :presence => true, numericality: true
  validates :event_date, :presence => true
  validate :valid_date?
  validates :seat_count, :presence => true, numericality: true
  validates :rows, :presence => true, numericality: true
  validate :seat_row?
  validate :rows_dividable?

  validates :image, attachment_presence: true
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  has_many :tickets

  def seat_row?
    if seat_count/rows > 25
      errors.add('Seat count','maximum of 25 seats in a row')
    end
  end

  def rows_dividable?
    if seat_count % rows != 0
      errors.add('Seat count', 'needs to dividable by rows')
    end
  end

  def valid_date?
    if event_date.past?
      errors.add('Event date','cannot be from the past')
    end
  end

  def seats_left
    seat_count - tickets.flat_map{|t| [t.seat1,t.seat2,t.seat3,t.seat4,t.seat5]}.select{|seat| !seat.blank?}.length
  end

  def is_today
    if event_date == Date.today
      return true
    end
    false
  end
end
