class Ticket < ApplicationRecord
  validates :name, :presence => true, :length => { :minimum => 6}
  validates :email_address, :presence => true
  validates :price, :presence => true
  validate :got_money?
  validate :enought_seat_slots?
  validate :any_seat_taken?

  belongs_to :event
  belongs_to :user

  def calculate_price
    seats = [seat1,seat2,seat3,seat4,seat5].select{|seat| !seat.blank?}.length
    event_price = event.price
    multiplier = 1.0
    if event.event_date == Date.today
      multiplier = 1.2
    end
    seats * event_price * multiplier
  end

  def got_money?
    if price > user.money
      errors.add('No Money ','no Funny, Honey Bunny')
    end
  end

  def available_seats?
    count = 5
    if event.blank?
      return count
    end
    used_seats = user.tickets.select{|ticket| ticket.event.id == event.id}
                     .flat_map{|a| [a.seat1, a.seat2, a.seat3, a.seat4, a.seat5]}
                     .select{|seat| !seat.blank?}
    count - used_seats.length
  end

  def enought_seat_slots?
    seats = [seat1,seat2,seat3,seat4,seat5]
    av_seats = available_seats?
    if seats.select{|seat| !seat.blank?}.length > av_seats
      errors.add('Available seats count is', av_seats)
    end
  end

  def any_seat_taken?
    seats = [seat1,seat2,seat3,seat4,seat5]
    if seats.select{|seat| !seat.blank?}.length == 0
      errors.add('No seat ', 'choosen')
    end
  end

end


