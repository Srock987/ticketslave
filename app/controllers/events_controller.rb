class EventsController < ApplicationController
  before_action :admin_user, only: [:new, :create, :destroy]

  def index
    @events = Event.where('event_date >= ?', Date.today ).order(event_date: :asc)
    @archive_events = Event.where('event_date < ?', Date.today )
  end

  def show
    @event = Event.find(params[:id])
    @tickets = Ticket.where(:event_id => @event.id)
    @ticket = Ticket.new(:event_id => @event.id, :price => 0.0)
      if current_user
        @ticket.user_id = current_user.id
        @ticket.email_address = current_user.email
      end
  end

  def new
    @event = Event.new
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @event = Event.new(event_params)

    if @event.save
      flash[:komunikat] = 'Event was successfully created.'
      redirect_to "/events/#{@event.id}"
    else
      render 'new'
    end

  end

  def destroy
    if !current_user.is_admin
      redirect_to events_path, alert: "Only admistrator can remove events!"
      return
    end

    @event = Event.find(params[:id])
    @event.ticket.each{|t| t.destroy}
    @event.destroy

    redirect_to events_path
  end

  private
  # Use callbacks to share common setup or constraints between actions.

  def admin_user
    if !current_user.is_admin
      redirect_to events_path, alert: "Only administrator can perform this action!"
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def event_params
    params.require(:event).permit(:artist, :description, :price, :event_date, :image, :adults_only, :seat_count, :rows)
  end



end
