class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show ]
  before_action :authenticate_user!, except: [:index, :show]
  before_action :correct_user, only:  [:edit,:update, :destroy]

  # GET /tickets
  # GET /tickets.json
  def index
    @event = Event.find(params[:event_id])
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
  end

  def show_user_tickets
    @tickets = Ticket.where(:user_id => params[:id])
  end

  def show_arena
    @provided_tickets = Ticket.where(:event_id => params[:id])
    p @provided_tickets
    @provided_event = Event.find(params[:id])
    p @provided_event
    respond_to do |format|
      format.js
    end
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new(:user_id => current_user.id, :email_address => current_user.email, :price => 0.0)
  end

  # GET /tickets/1/edit
  def edit
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Ticket.new(ticket_params)
      respond_to do |format|
        if @ticket.save
          @user = @ticket.user
          @user.money -= @ticket.price
          @user.save
          format.html { redirect_to @ticket, notice: 'Ticket was successfully created.' }
          format.json { render :show, status: :created, location: @ticket }
        else
          format.html { render :new }
          format.json { render json: @ticket.errors, status: :unprocessable_entity }
        end
      end

  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    respond_to do |format|
      if @ticket.update(ticket_params)
        format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.html { render :edit }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    refund_ticket(@ticket)
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to my_tickets_url, notice: 'Ticket was successfully refunded.' }
      format.json { head :no_content }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end


    def refund_ticket(ticket)
      @date = ticket.event.event_date
      @date_difference = @date - DateTime.now.to_date
      if @date_difference > 0
        @cut_down_rate = 6
        @week_difference = @date_difference/(24*3600*7)
        while @week_difference > 1 && @cut_down_rate > 1
          @cut_down_rate -= 1
        end
        @user = ticket.user
        @user.money += ticket.price - ticket.price * (@cut_down_rate/10.0)
        @user.save
      end
    end

    def correct_user
      @ticket = current_user.tickets.find_by(id: params[:id])
      redirect_to my_tickets_url, notice: "You are not authorized to edit this ticket" if @ticket.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:name, :address, :email_address, :price, :phone, :event_id, :user_id, :seat1, :seat2, :seat3, :seat4, :seat5)
    end
end
