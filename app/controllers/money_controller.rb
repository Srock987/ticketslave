class MoneyController < ApplicationController
  before_action :authenticate_user!

  def index

  end

  def create
    @user = User.find(current_user.id)
    @user.money += 1000
    @user.save
    redirect_to show_me_my_money_path, notice: "New founds has been added to your balance"
  end



end