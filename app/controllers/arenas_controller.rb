class ArenasController < ApplicationController
  def show
    @provided_event = Event.find(:id => params[:event_id])
    @provided_tickets = Ticket.where(:event_id => params[:event_id])
    respond_to do |format|
      format.js
    end
  end
end
