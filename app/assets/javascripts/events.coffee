# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
seats = ["#ticket_seat1","#ticket_seat2","#ticket_seat3","#ticket_seat4","#ticket_seat5"]
seatSelectedColor = '#0ebb09'
seatFreeColor = '#9f9fa3'

@isTaken = (tickets) ->
  console.log(tickets)
  for ticket in tickets
    if ticket.seat1?
      $("#seat_#{ticket.seat1}").addClass('taken')
    if ticket.seat2?
      $("#seat_#{ticket.seat2}").addClass("taken")
    if ticket.seat3?
      $("#seat_#{ticket.seat3}").addClass("taken")
    if ticket.seat4?
      $("#seat_#{ticket.seat4}").addClass("taken")
    if ticket.seat5?
      $("#seat_#{ticket.seat5}").addClass("taken")

@addSeat = (element, row_id, seat_id, eventPrice, today) ->
  if !element.classList.contains("taken")
    if today
      eventPrice = eventPrice * 1.2
    for i in seats
      if $(i).val() == "#{row_id}.#{seat_id}"
        $(i).val('')
        element.style.backgroundColor = seatFreeColor
        price = parseFloat($("#ticket_price").val())
        price -= eventPrice
        $("#ticket_price").val(price)
        return
    for i in seats
      if  !$(i).val()
        $(i).val("#{row_id}.#{seat_id}")
        element.style.backgroundColor = seatSelectedColor
        price = parseFloat($("#ticket_price").val())
        price += eventPrice
        $("#ticket_price").val(price)
        return
  else
    console.log("Seat is taken")