Rails.application.routes.draw do

  devise_for :users

  get 'events/index', to: 'events#index', as: 'events'

  get 'events/new', to: 'events#new', as: 'new_event'

  post 'events/index', to: 'events#create'

  get 'events/:id', to: 'events#show'

  get 'tickets/mytickets/:id', to: 'tickets#show_user_tickets', as: 'my_tickets'

  get 'money/index', to: 'money#index', as: 'show_me_my_money'

  post 'money/create', to: 'money#create', as: 'give_me_my_money'

  get 'tickets/arena/:id', to: 'tickets#show_arena', as: 'show_arena'

  root :to => 'events#index'

  resources :tickets
  resources :events, :only => [:index, :new, :create, :show]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
