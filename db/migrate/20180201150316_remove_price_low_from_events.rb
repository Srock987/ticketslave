class RemovePriceLowFromEvents < ActiveRecord::Migration[5.1]
  def change
    remove_column :events, :price_low
  end
end
