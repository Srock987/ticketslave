class AddSeatsToTickets < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :seat1, :string
    add_column :tickets, :seat2, :string
    add_column :tickets, :seat3, :string
    add_column :tickets, :seat4, :string
    add_column :tickets, :seat5, :string
    remove_column :tickets, :seat_id_seq
  end
end
