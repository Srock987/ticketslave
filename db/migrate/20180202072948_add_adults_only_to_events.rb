class AddAdultsOnlyToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :adults_only, :boolean, :default => false
  end
end
