class AddRowsToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :rows, :integer
  end
end
