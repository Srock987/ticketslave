class AddSetCountToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :seat_count, :integer
  end
end
