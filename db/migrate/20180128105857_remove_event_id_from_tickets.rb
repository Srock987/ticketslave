class RemoveEventIdFromTickets < ActiveRecord::Migration[5.1]
  def change
    remove_column :tickets, :eventId
  end
end
