class ChangePriceInTickets < ActiveRecord::Migration[5.1]
  def change
    remove_column :tickets, :price
    remove_column :events, :price_low
    remove_column :events, :price_low
    add_column :events, :price, :decimal, :default => 100.0
  end
end
