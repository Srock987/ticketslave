class MoveAdultsToEvents < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :adults_only
    add_column :events, :adults_only, :boolean, :default => false
  end
end
