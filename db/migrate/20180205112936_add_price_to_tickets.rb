class AddPriceToTickets < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :price, :decimal
  end
end
