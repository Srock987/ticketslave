class RemoveDefValOfPriceFromEvents < ActiveRecord::Migration[5.1]
  def change
    change_column_default(:events,:price, nil)
  end
end
