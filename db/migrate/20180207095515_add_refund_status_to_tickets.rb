class AddRefundStatusToTickets < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :refund_status, :boolean, :default => false
  end
end
